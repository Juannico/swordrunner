using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Puntaje : MonoBehaviour
{
    private int puntaje;
    public Text score;
    public Text win;
    public Text highScore;
    public GameObject interfazInicio;
    public bool primeraEscena;

    // El juego inicia en cero, cuando el usuario da nueva partidao juega por primera vez se borran los datos.
    // El juego conservara el puntaje cuando el usuario le de "guardar" y se carga a la siguiente partida.


    void Awake()
    {
        puntaje = 0;
        win.gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        if (!primeraEscena)
        {
            highScore.text = "HighScore: " + PlayerPrefs.GetInt("HighScore").ToString();
            puntaje = PlayerPrefs.GetInt("HighScore");
            highScore.text = puntaje.ToString();
            Time.timeScale = 1; // Escala de tiempo.
        }
        else
        {
            Time.timeScale = 0; // Congelar el inicio del juego.
        }
    }
    // Update is called once per frame
    void Update()
    {
       
    }
    void OnTriggerEnter(Collider obj)//Entrar a un collider, que activa un trigger que se activa por contacto.
    {
        if (obj.tag == "cajas")//Crear un tag para las cajas en Unity.
        {
            obj.gameObject.SetActive(false);
            puntaje++; //puntuacion + = 1
            actualizarPuntos();
        }
        if (obj.tag == "meta") // Crear un tag para la Meta en Unity.
        {
            print("META");
            win.gameObject.SetActive(true);
            puntajeFinal();// Metodo de llamado a la funcion de puntaje final.
            StartCoroutine(esperarCambioDeEscena()); //Metodo de llamado, llama la funcion corrutina(IEnumerator) funciones para esperar.        
        }
    }

    IEnumerator esperarCambioDeEscena() //Esperar el cambio de escena, determinar tiempo.
    {
        yield return new WaitForSeconds(2.0f);
        if (SceneManager.GetActiveScene().name == "Prototype 1") // Si la escena es la 1, no pasa de la escena 2
        {
            SceneManager.LoadScene(1);
        }
    }
    public void reset() //Funcion para borrrar los playerprefs(preferencias del jugador).
    {
        PlayerPrefs.DeleteAll();
        highScore.text = "0";
        Time.timeScale = 1; // En 1 corre en tiempo real.
        interfazInicio.SetActive(false);
    }
    public void loadGame()
    {
        highScore.text = "HighScore: " + PlayerPrefs.GetInt("HighScore").ToString();
        puntaje = PlayerPrefs.GetInt("HighScore");
        highScore.text = puntaje.ToString();
        Time.timeScale = 1; // En 1 corre en tiempo real.
        interfazInicio.SetActive(false); // Apaga la interfaz de inicio.
    }
    public void puntajeFinal() //Escribir el puntaje en la interfaz.
    {
        highScore.text = "HighScore: " + puntaje.ToString();
    }
    public void actualizarPuntos() //Actualizar puntos durante el juego, cada que se toque una caja.
    {
        int number = (puntaje);
        score.text = "Score: " + puntaje.ToString();

        if (number > PlayerPrefs.GetInt("HighScore", 0)) //Si este numero es mayor que el puntaje mayor actual se actualiza.
        {
            PlayerPrefs.SetInt("HighScore", number);
        }
    }
}
