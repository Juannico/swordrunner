using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    // Las variables estan plegadas, solo es desplegarlas desde el + al lado derecho de la numeracion de la linea(linea 9) donde dice Variables

    #region Variables

    // Grupo de objetos que se usaran en el nivel
    public Dictionary<string, Queue<GameObject>> Object;
    // Lista en la que se asignaran los tipos de objetos que apareceran en el nivel
    public List<Pool> SpawnerAtributes; // pool es una clase que contiene diferentes variables guardadas.
    // Variable por la cual se accede a las funcioens del spawner
    public static SpawnerManager Instance;
    // Variable usada como indice para la lista generada a partir de la clase Pool
    int index;
    // Variable usada como indice para la variable de posiciones de la clase Pool
    int Pos;
    private List<int> lastPos = new List<int>();
    #endregion

    private void Awake()
    {
        // Funci�n encargada de hacer que la variable pueda acceder a las funciones del script

        Instance = this;
    }
    void Start()
    {
        // Los objetos a instanciar son una cola para usar en la lista

        Object = new Dictionary<string, Queue<GameObject>>();


        // por cada tipo en la lista de tipos creara una cola del objeto correspondiente
        

        foreach (Pool Atributes in SpawnerAtributes)
        {
            // Cola creada

            Queue<GameObject> ObjectType = new Queue<GameObject>();

            // por medio de este bucle se asegura que el script agregue el objeto en la cola y genera un numero aleatorio en el rango entre 0 y el total de posiciones
            // que se usa como indice de la lista de posiciones para hacer aparecer los objetos en posiciones aleatorias

            for (int i = 0; i < Atributes.positions.Length; i++)
            {
                lastPos.Add(i);
                //    Pos = Random.Range(0, Atributes.positions.Length);

                // Si el numero aleatorio generado en el rango desde 0 hasta el total de la lista de posiciones es igual a Pos entonces genere otro numero aleatorio de ese mismo rango
                /*  while (lastPos.Contains(Pos))
                  {
                      Pos = Random.Range(0, Atributes.positions.Length);
                  }*/
                //lastPos[i] = Pos;
                //print("Pos" + Pos);
                GameObject SpawnObj = Instantiate(Atributes.prefab);
                SpawnObj.SetActive(false);
                ObjectType.Enqueue(SpawnObj); //Enqueue poner en cola
            }
          

            // El objeto es a�adido al grupo de objetos

            Object.Add(Atributes.tag, ObjectType);
        }
        for (int i = 0; i < SpawnerAtributes.Count; i++)
        {
            //Pos = Random.Range(0, SpawnerAtributes.Count - 1);
            //int value = lastPos[Pos];
            //lastPos.RemoveAt(Pos);
            /*while (lastPos.Contains(Pos))
                {
                    Pos = Random.Range(0, SpawnerAtributes.Count - 1);
                }*/

            lastPos[i] = NotRepeatRandom();
            print("Pos" + Pos);
        }
    }

    int NotRepeatRandom() // funcion entera
    {
        //for (int i = 0; i < SpawnerAtributes.Count; i++)
        //{
        if (lastPos.Count == 0)
        {
            return -1;
        }
        Pos = Random.Range(0, SpawnerAtributes.Count);
            int value = lastPos[Pos];
            lastPos.RemoveAt(Pos);
            /*while (lastPos.Contains(Pos))
                {
                    Pos = Random.Range(0, SpawnerAtributes.Count - 1);
                }*/
            //lastPos[i] = Pos;
            //print("Pos" + Pos);
            return value;
        //}
        
    } 

    public GameObject SpawnObject(string tag)
    {
        // Funci�n encargada de de coordinar la aparici�n y reutilizaci�n de los objetos

        // Si el tag del objeto no es de un objeto que haga parte de un grupo de objetos entonces indicara por consola que el objeto no existe

        if (!Object.ContainsKey(tag))
        {
            print("El objeto con el tag" + tag + "no existe");
            return null;
        }

        // Si el objeto instanciado esta activo en la jerarquia entonces devuelva un valor 

        if (Object[tag].Peek().activeInHierarchy)
        {
            return null;
        }

        // El objeto saldra de la cola y aparecera en la posicion y rotacion que se le asigne al objeto por medio de emptys de referencia

        GameObject ObjectToSpawn = Object[tag].Dequeue(); //Dequeue sacar de la cola
        ObjectToSpawn.SetActive(true);
        ObjectToSpawn.transform.position = SpawnerAtributes[index].positions[Pos].position + SpawnerAtributes[index].offset;
        ObjectToSpawn.transform.rotation = SpawnerAtributes[index].positions[Pos].rotation;

        if (ObjectToSpawn.transform.position.x < 0) {
            ObjectToSpawn.GetComponent<EnemyProperties>().id = 0;
        } else if (ObjectToSpawn.transform.position.x >= 0){
            ObjectToSpawn.GetComponent<EnemyProperties>().id = 1;
        }
        

        // Por medio de este bucle se recorre la lista de posiciones y se genera un numero aleatorio en un rango desde 0 hasta el total de la lista de posiciones
        // que se usa como indice de la lista de posiciones para hacer aparecer los objetos en posiciones aleatorias

        for (int i = 0;i < SpawnerAtributes[index].positions.Length; i++)
        {
            Pos = Random.Range(0, SpawnerAtributes[index].positions.Length);

            // Si el numero aleatorio generado en el rango desde 0 hasta el total de la lista de posiciones es igual a Pos entonces genere otro numero aleatorio de ese mismo rango

            if (Random.Range(0, SpawnerAtributes[index].positions.Length) == Pos)
            {
                Pos = Random.Range(Pos, SpawnerAtributes[index].positions.Length);
            }
        }

        // Variable que obtiene los componentes(funciones,voids asignados) de la interfaz

        IFunctions EnemyFunctions = ObjectToSpawn.GetComponent<IFunctions>();

        // Si hay una funci�n en la interfaz ejecute la funci�n

        if (EnemyFunctions != null)
        {
            EnemyFunctions.EnemyAtack();
            EnemyFunctions.EnemySight();
        }

        // el objeto desaparecera y volvera a la cola

        Object[tag].Enqueue(ObjectToSpawn);

        return ObjectToSpawn;
    }

    // Clase que da propiedades al script �NO MODIFICAR SI NO ENTIENDE EL SCRIPT!

    [System.Serializable]
    public class Pool // Object pooling, reciclar recursos para optimizar el juego, objetos finitos de uso ilimitado.
    {
        [Tooltip("Tag del objeto que aparecera")]
        public string tag;
        [Tooltip("Objeto que aparecera")]
        public GameObject prefab;
        [Tooltip("Numero de objetos que apareceran")]
        public int size;
        [Tooltip("Emptys usados para asignar posiciones de aparici�n de los objetos")]
        public Transform[] positions;
        [Tooltip("Vector usado para a�adir separaci�n a la posici�n de aparicion del objeto")]
        public Vector3 offset;
    }
}
