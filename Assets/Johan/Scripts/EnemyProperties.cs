using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProperties : MonoBehaviour, IFunctions
{
    // Las variables estan plegadas, solo es desplegarlas desde el + al lado derecho de la numeracion de la linea(linea 9) donde dice Variables

    #region Variables

    // Las siguientes funciones solo son para propositos esteticos y de orden en el inspector de unity y no afectan la funci�n del c�digo en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)

    // Variable con la que se accede al script del SpawnManager
    SpawnerManager Enemies;
    [Space(5)]
    [Tooltip("El jugador que sera atacado")]
    public Transform player;
    [Space(5)]
    [Tooltip("El rango de vision que tendra el objeto")]
    public float range;
    [Space(5)]
    [Tooltip("Variable usada para a�adir un tiempo de retraso antes que de el objeto ejecute un ataque")]
    public float AtackDelay;
    // variable usada para crear un raycast que le da vision al objeto
    Ray sight;
    public int id;
    public bool canAttack;
    public bool attackOnce;
    bool startMove;
    float speed = 10.0f;

    #endregion

    void Start()
    {
        canAttack = true;  
        // La variable ahora accede a las funciones del script del SpawnManager 

        Enemies = SpawnerManager.Instance;
        player = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {
        // La variable accede a la funci�n que coordina la aparici�n de los objetos con la posici�n y rotaci�n de los objetos

        Enemies.SpawnObject("Enemy");

        EnemySight();
        EnemyAtack();
        //transform.position = player.position;
        //transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        //var step =  speed * Time.deltaTime;
        if (startMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
            float distance = Vector3.Distance(transform.position, player.position);
            if(distance <= 0.1f)
            {
                startMove = false;
                gameObject.SetActive(false);
            }
        }

    }

    public void EnemySight()
    {
        // Funci�n encargadad de darle vision al enemigo

        sight = new Ray(transform.position, transform.forward * range);
    }

    public void EnemyAtack()
    {
        // Funci�n encargada del ataque del enemigo

        // Si hay un raycast entonces ejecute una condici�n

        if (Physics.Raycast(transform.position, transform.forward, range)) //raycast: ataque por distancia
        {
            // linea para dibujar el raycast en unity
            Debug.DrawRay(transform.position, transform.forward, Color.red);

            // Si la distancia entre el jugador y el raycast es menor al rango de visi�n entonces mire al jugador e incie una corrutina

            if (Vector3.Distance(transform.position, player.position) < range)
            {
                transform.LookAt(player);
                if (!attackOnce)
                {
                    StartCoroutine(Delay());
                }
                
            }
        }
    }

    IEnumerator Delay()
    {
        // Corrutina que coordina el retraso del enemigo antes de atacar asi como su ataque

        // a�ade un retraso
        // despues del retraso se imprime un mensaje en consola
        // la posici�n del enemigo es igual a la posici�n del jugador
        attackOnce = true;
        yield return new WaitForSeconds(AtackDelay);
        //print("atacando");
        if (canAttack)
        {
            startMove = true;
            yield return new WaitForSeconds(0.7f);
            if (canAttack)
            {
                canAttack = false;
                player.GetComponent<PlayerProperties>().health -= 20;
            }
            
        }
        yield return new WaitForSeconds(0.9f);
        //player.GetComponent<PlayerProperties>().dieonce = false;
        gameObject.SetActive(false);
    }
}
// mirar si hay enemigos duplicados para quitarlos
// analizar ataque