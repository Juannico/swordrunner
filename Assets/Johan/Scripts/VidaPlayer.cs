using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class VidaPlayer : MonoBehaviour
{
    public float vida = 100;

    public Image barraDeVida;

    // Update is called once per frame
    void Update()
    {
      
        vida = (float)gameObject.GetComponent<PlayerProperties>().health;

        barraDeVida.fillAmount = vida / 100;

        print(barraDeVida.fillAmount);

        
    }
}
