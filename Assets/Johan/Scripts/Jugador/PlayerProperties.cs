using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerProperties : MonoBehaviour
{
    // Las variables estan plegadas, solo es desplegarlas desde el + al lado derecho de la numeracion de la linea(linea 9) donde dice Variables

    #region Variables


    // Las siguientes funciones solo son para propositos esteticos y de orden en el inspector de unity y no afectan la funci�n del c�digo en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)

    #region Variables del jugador

    [Space(10)]
    [Header("Variables del jugador")]

    [Space(5)]
    [Tooltip("la velocidad con la que se movera el jugador")]
    public float speed;
    [Space(5)]
    [Tooltip("El rigidbody que usar� el jugador")]
    public GameObject Player;
    [Space(5)]
    [Tooltip("La vida del jugador")]
    public int health;


    // Variable usada para acceder al script de SpawnerManager
    SpawnerManager enemy;
    public bool dieonce;
    bool enemyAttackOnce;
    bool rightState;
    bool leftState;
    float timer;
    float limitTime = 1.0f;
    float timerLeft;
    #endregion

    #endregion
    void Start()
    {
        // La variable accede al script de SpawnerManager
        enemy = SpawnerManager.Instance;

        // Se obtiene el rigidbody y el mesh renderer del jugador
        Player.GetComponent<Rigidbody>();
        Player.GetComponent<Renderer>();
    }

    void FixedUpdate()
    {
        MovPlayer();
    }

    void Update()
    {
        SwipePlayer();
        KillPlayer();
    }

    void MovPlayer()
    {
        // Funci�n encargada del movimiento frontal del jugador

        Vector3 MovFront = transform.forward * speed * Time.fixedDeltaTime;
        Player.GetComponent<Rigidbody>().MovePosition(Player.transform.position + MovFront);
    }


    void SwipePlayer()
    {
        // Funci�n encargada de coordinar las acciones del jugador cuando hace un deslizamiento con el dedo en la pantalla

        // Si el jugador desliza el dedo a la izquierda muestre un mensaje en consola

        if (SwipeManager.swipeLeft)
        {
            leftState = true;
            print("swipe izquierda");
        }

        // Si el jugador desliza el dedo a la derecha muestre un mensaje en consola

        if (SwipeManager.swipeRight)
        {
            rightState = true;
            print("swipe derecha");
        }

        // Si el jugador desliza el dedo al frente muestre un mensaje en consola

        if (SwipeManager.swipeUp)
        {
            print("swipe frente");
        }
    }

    void KillPlayer()
    {
        // Funci�n encargada de la muerte del jugador

        // Si la salud del jugador es 0 entonces muestre un mensaje en consola, la velocidad sera de 0 y el material del jugador cambiara a color rojo

        if (health <= 0)
        {
            print("jugador muerto");
            speed = 0;
            Player.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        }
    }

    void OnTriggerStay(Collider other) //OntriggerStay se actualiza en todos los frames que esten dentro del triger
    {
        // Trigger encargado del da�o que recibe el jugador de un enemigo
        // Si el jugador colisiona con un objeto con el tag Enemy entonces la salud bajara una unidad y el material del jugador cambiara a color amarillo

        if (other.GetComponent<Collider>().tag == "Enemy")
        //Si el jugador no se mueve el enemigo ataca
        //prints de porque no bajan la salud del persoje principal
        {
            print("trigger Enemy");
            //dieonce = false;
            if (!enemyAttackOnce)
            {
                enemyAttackOnce = true;
                //StartCoroutine(DelayEnemyAttack());
            }

            if (rightState)
            {
                timer += Time.deltaTime;
                print("Ataque derecho Trigger");
                if (other.GetComponent<EnemyProperties>().id == 1 && !dieonce)
                {
                    other.GetComponent<EnemyProperties>().canAttack = false;
                    //StopCoroutine(DelayEnemyAttack());
                    other.gameObject.SetActive(false);
                    print("ataque derecho"); //Cuantos frames dura el ataque
                }
                else if (other.GetComponent<EnemyProperties>().id == 0 && !dieonce)
                {
                    dieonce = true;
                    //StartCoroutine(DelayEnemyAttack());
                    //other.gameObject.SetActive(false);
                }
                if (timer >= limitTime)
                {
                    rightState = false;
                    timer = 0;
                }
            }
            else if (leftState)
            {
                timerLeft += Time.deltaTime;
                print("Ataque izquierdo Trigger");
                if (other.GetComponent<EnemyProperties>().id == 0 && !dieonce)
                {
                    other.GetComponent<EnemyProperties>().canAttack = false;
                    //StopCoroutine(DelayEnemyAttack());
                    print("ataque izquierdo"); //Cuantos frames dura el ataque
                    other.gameObject.SetActive(false);
                }

                else if (other.GetComponent<EnemyProperties>().id == 1 && !dieonce)
                {
                    dieonce = true;
                    //StartCoroutine(DelayEnemyAttack());
                    //other.gameObject.SetActive(false);               
                }
                if (timerLeft >= limitTime)
                {
                    leftState = false;
                    timerLeft = 0;
                }

            }
        }
        //Hacer que el jugador no se auto ataque y pueda detener el ataque enemigo
        IEnumerator DelayEnemyAttack()
        {
            yield return new WaitForSeconds(2);
            //health -= 20;
            Player.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
        }
    }
}
