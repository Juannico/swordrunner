using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeManager : MonoBehaviour
{
    // Script para crear el swipe y usar sus funciones con el movimiento del jugador, �NO MODIFICAR!
    
    public static bool tap, swipeLeft, swipeRight, swipeUp, swipeDown; // variables de todos los movimientos posibles en la pantalla
    private bool isDraging = false; //isDraging (si se esta moviendo el dedo sobre la pantalla)
    private Vector2 startTouch, swipeDelta; // vector X/Y comienzo toque de toque pantalla con las unidades de desplazamiento

    private void Update()
    {
        tap = swipeDown = swipeUp = swipeLeft = swipeRight = false; // tap variable toque, todas las posiciones se igualan a falso

        //region fragmento de codigo que solo se ejecuta en dispositivo especifico (PC)
        #region Standalone Inputs 
        if (Input.GetMouseButtonDown(0)) //Oprimir el boton izquierdo del mouse
        {
            tap = true;
            isDraging = true;
            startTouch = Input.mousePosition; // Posicion inicial del toque, su ubicacion en pantalla
        }
        else if (Input.GetMouseButtonUp(0)) // Soltar el boton de click
        {
            isDraging = false;
            Reset();
        }
        #endregion

        //region fragmento de codigo que solo se ejecuta en dispositivo especifico (CELULAR)
        #region Mobile Input
        if (Input.touches.Length > 0) // Cantidad de toques en la pantalla en simultaneo, maximo 50
        {
            if (Input.touches[0].phase == TouchPhase.Began) // Detecta cuando comienza el touch
            {
                tap = true;
                isDraging = true;
                startTouch = Input.touches[0].position; // La posicion de donde se hizo el primer touch(primer dedo en tocar)
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled) // Dejar de tocar la pantalla
            {
                isDraging = false;
                Reset(); // se reinicia la funcion
            }
        }
        #endregion

        //Calculate the distance
        swipeDelta = Vector2.zero; // cero extremo parte superior izquiera de la pantalla
        if (isDraging) // si el dedo se esta desplazando
        {
            if (Input.touches.Length < 0) // Si la cantidad de toques es menor a cero, significa que no esta tocando la pantalla
                swipeDelta = Input.touches[0].position - startTouch; // desplazamiento es posicion actual, menos posicion inicial
            else if (Input.GetMouseButton(0)) // oprime y suelta el click izquierdo (0), (1) click derecho, (2) rueda del mouse
                swipeDelta = (Vector2)Input.mousePosition - startTouch; // parse variable, es forzar la variable a cambiar su tipo de dato (Vector2), posicion de mouse menos posicion inicial
        }

        //Did we cross the distance?
        if (swipeDelta.magnitude > 100) //magnitude, distancia entre dos puntos vectoriales
        {
            //Which direction?
            float x = swipeDelta.x;
            float y = swipeDelta.y;
            if (Mathf.Abs(x) > Mathf.Abs(y)) // Abs es valor absoluto, Mathf libreria de C con operaciones matematicas //
            {
                // Si hay mas distancia recorrida en (X) que en (Y) hacemos lo siguiente...

                //Left or Right
                if (x < 0)
                    
                {
                   swipeLeft = true;
                    
                }
                else
                    
                {
                   swipeRight = true;
                 
                }
                
            }
            else // El valor absoluto, si hay mas distancia recorrida (Y) ignoramos en (X)
            {
                //Up or Down
                if (y < 0)
                    swipeDown = true;
                else
                    swipeUp = true;
            }

            Reset();
        }

    }

    private void Reset() // Toque inicial y desplazamiento inicial comienzan en cero
    {
        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
    }
}
