using UnityEngine;

// Interfaz encargada de hacer que las funciones que se le asignan funcionen correctamente sin importar el objeto �NO MODIFICAR SI NO ENTIENDE EL SCRIPT!
public interface IFunctions
{
    // Se agregan las funciones que deben funcionar en todo momento sin importar de que script sean, la funci�n que se agrega debe ser publica para que funcione

    void EnemySight();
    void EnemyAtack();
    
}

